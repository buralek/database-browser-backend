FROM openjdk:15

ADD ./target/database-browser-backend-0.0.1-SNAPSHOT.jar /app/
CMD ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:2110", "-jar", "/app/database-browser-backend-0.0.1-SNAPSHOT.jar"]

EXPOSE 2010