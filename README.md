# database-browser
This is a simple database browser backend application

**Task description**

**Task 1:**
Implement backend for saving, updating, listing and deleting connection details to you favourite
relational database.

E.g. If you choose the MySQL database, you should persist at least the following properties:

1) name - custom name of the database instance

2) hostname - hostname of the database

3) port - port where the database runs

4) databaseName - name of the database

5) username - username for connecting to the database

6) password - password for connecting to the database

Connection details themselves should be stored in database of your choice.

**Task 2:**
Design and implement REST API for browsing structure and data using your stored database

connections from Task 1. Your API should support the following operations:

1) Listing schemas (if your selected database supports them)

2) Listing tables

3) Listing columns

4) Data preview of the table

Resources should contain as much information you can find (data type, if it is primary key, etc.).

**Bonus tasks:**
Design and implement REST API endpoints for statistics:

1) Single endpoint for statistics about each column: min, max, avg, median value of the
column.

2) Single endpoint for statistics about each table: number of records, number of attributes.

3) Document this REST API

**Solution description**

I've realized all tasks except "Single endpoint for statistics about each column: min, max, avg, median value of the
column". Because we need to create a generalized solution, we need to get column type and only after that  we
can calculate min max and etc(if it's possible for this type). It's not hard and may be made in the future.

There are 3 services:

1) _database-browser-backend_ - This is a backend service which realize tasks

2) _database-browser-db_ - This is a database where we keep databases connection details
    
3) _sample-db_ - This is a sample database from which we try to catch information

**How to use**
1) Go to root directory
2) Build project

   _mvn clean install_
3) Build and start docker-compose

   _docker-compose up_
4) Go to database-browser swagger
http://localhost:2010/database-browser/swagger-ui.html



