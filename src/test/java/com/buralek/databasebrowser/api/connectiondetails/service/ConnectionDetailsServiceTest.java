package com.buralek.databasebrowser.api.connectiondetails.service;

import com.buralek.databasebrowser.api.connectiondetails.dto.ConnectionDetailDto;
import com.buralek.databasebrowser.api.connectiondetails.dto.CreateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.dto.UpdateConnectionDetailRequest;
import com.buralek.databasebrowser.service.crypt.ObjectCrypter;
import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;
import com.buralek.databasebrowser.service.database.repository.ConnectionDetailsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static javax.xml.bind.DatatypeConverter.printHexBinary;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class ConnectionDetailsServiceTest {
    private final Integer ID = 1;
    private final String NAME = "testDB";
    private final String HOSTNAME = "hostname";
    private final String PORT = "1234";
    private final String DATABASE_NAME = "database_test";
    private final String USERNAME = "test_user";
    private final String PASSWORD = "test_password";

    @Autowired
    private ConnectionDetailsService connectionDetailsService;

    @Autowired
    private ObjectCrypter objectCrypter;

    @MockBean
    private ConnectionDetailsRepository connectionDetailsRepository;

    @Test
    void getAllConnectionDetailsTest() {
        when(connectionDetailsRepository.findAll()).thenReturn(getConnectionDetailEntities());

        List<ConnectionDetailDto> connectionDetailDtoList = connectionDetailsService.getAllConnectionDetails();

        assertNotNull(connectionDetailDtoList);
        assertEquals(connectionDetailDtoList.size(), 1);

        ConnectionDetailDto connectionDetailDto = connectionDetailDtoList.get(0);
        assertEquals(connectionDetailDto.getId(), ID);
        assertEquals(connectionDetailDto.getName(), NAME);
        assertEquals(connectionDetailDto.getHostname(), HOSTNAME);
        assertEquals(connectionDetailDto.getDatabaseName(), DATABASE_NAME);
        assertEquals(connectionDetailDto.getPort(), PORT);
        assertEquals(connectionDetailDto.getUsername(), USERNAME);
        assertEquals(connectionDetailDto.getPassword(), PASSWORD);
    }

    @Test
    void getConnectionDetailByIdTest() {
        when(connectionDetailsRepository.findById(ID)).thenReturn(Optional.of(getConnectionDetailEntity()));

        ConnectionDetailDto connectionDetailDto = connectionDetailsService.getConnectionDetailById(ID);

        assertEquals(connectionDetailDto.getId(), ID);
        assertEquals(connectionDetailDto.getName(), NAME);
        assertEquals(connectionDetailDto.getHostname(), HOSTNAME);
        assertEquals(connectionDetailDto.getDatabaseName(), DATABASE_NAME);
        assertEquals(connectionDetailDto.getPort(), PORT);
        assertEquals(connectionDetailDto.getUsername(), USERNAME);
        assertEquals(connectionDetailDto.getPassword(), PASSWORD);
    }

    @Test
    void createConnectionDetailTest() {
        CreateConnectionDetailRequest request = new CreateConnectionDetailRequest();
        when(connectionDetailsRepository.save(any())).thenReturn(getConnectionDetailEntity());

        ConnectionDetailDto connectionDetailDto = connectionDetailsService.createConnectionDetail(request);

        verify(connectionDetailsRepository).save(any());
        assertEquals(connectionDetailDto.getId(), ID);
        assertEquals(connectionDetailDto.getName(), NAME);
        assertEquals(connectionDetailDto.getHostname(), HOSTNAME);
        assertEquals(connectionDetailDto.getDatabaseName(), DATABASE_NAME);
        assertEquals(connectionDetailDto.getPort(), PORT);
        assertEquals(connectionDetailDto.getUsername(), USERNAME);
        assertEquals(connectionDetailDto.getPassword(), PASSWORD);
    }

    @Test
    void deleteConnectionDetailByIdTest() {
        connectionDetailsService.deleteConnectionDetailById(ID);

        verify(connectionDetailsRepository).deleteById(ID);
    }

    @Test
    void updateConnectionDetailById() {
        when(connectionDetailsRepository.findById(ID)).thenReturn(Optional.of(getConnectionDetailEntity()));
        when(connectionDetailsRepository.save(any())).thenReturn(getConnectionDetailEntity());

        connectionDetailsService.updateConnectionDetailById(ID, new UpdateConnectionDetailRequest());

        verify(connectionDetailsRepository).findById(ID);
        verify(connectionDetailsRepository).save(any());
    }


    private List<ConnectionDetailEntity> getConnectionDetailEntities() {
        List<ConnectionDetailEntity> connectionDetailEntities = new ArrayList<>();
        connectionDetailEntities.add(getConnectionDetailEntity());
        return connectionDetailEntities;
    }

    private ConnectionDetailEntity getConnectionDetailEntity() {
        ConnectionDetailEntity connectionDetailEntity = new ConnectionDetailEntity();
        connectionDetailEntity.setId(ID);
        connectionDetailEntity.setName(NAME);
        connectionDetailEntity.setHostname(HOSTNAME);
        connectionDetailEntity.setPort(PORT);
        connectionDetailEntity.setDatabaseName(DATABASE_NAME);
        connectionDetailEntity.setUsername(printHexBinary(objectCrypter.encrypt(USERNAME)));
        connectionDetailEntity.setPassword(printHexBinary(objectCrypter.encrypt(PASSWORD)));
        return connectionDetailEntity;
    }


}
