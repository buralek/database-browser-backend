package com.buralek.databasebrowser.api.connectiondetails.controller;

import com.buralek.databasebrowser.api.connectiondetails.dto.CreateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.dto.UpdateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.service.ConnectionDetailsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ConnectionDetailsController.class)
public class ConnectionDetailsControllerTests {
    @MockBean
    private ConnectionDetailsService connectionDetailsService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllConnectionDetailsTest() throws Exception {
        this.mockMvc.perform(get("/connection-detail/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getConnectionDetailByIdTest() throws Exception {
        this.mockMvc.perform(get("/connection-detail").param("id", "123"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void createConnectionDetailTest() throws Exception {
        final ObjectMapper objectMapper = new ObjectMapper();
        final String request = objectMapper.writeValueAsString(new CreateConnectionDetailRequest());
        this.mockMvc.perform(post("/connection-detail").contentType(MediaType.APPLICATION_JSON).content(request))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void updatedConnectionDetailDtoTest() throws Exception {
        final ObjectMapper objectMapper = new ObjectMapper();
        final String request = objectMapper.writeValueAsString(new UpdateConnectionDetailRequest());
        this.mockMvc.perform(put("/connection-detail").contentType(MediaType.APPLICATION_JSON).content(request).param("id", "123"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteConnectionDetailById() throws Exception {
        this.mockMvc.perform(delete("/connection-detail").param("id", "123"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
