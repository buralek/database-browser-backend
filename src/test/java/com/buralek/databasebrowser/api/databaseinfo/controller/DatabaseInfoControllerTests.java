package com.buralek.databasebrowser.api.databaseinfo.controller;

import com.buralek.databasebrowser.api.databaseinfo.service.DatabaseInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DatabaseInfoController.class)
public class DatabaseInfoControllerTests {
    @MockBean
    private DatabaseInfoService databaseInfoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getSchemasTest() throws Exception {
        this.mockMvc.perform(get("/database-info/schemas").param("databaseId", "123"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getTablesTest() throws Exception {
        this.mockMvc.perform(get("/database-info/tables").param("databaseId", "123"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getColumnForTableTest() throws Exception {
        this.mockMvc.perform(get("/database-info/columns")
                .param("databaseId", "123").param("tableName", "test_table"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getTableDataPreviewTest() throws Exception {
        this.mockMvc.perform(get("/database-info/table-preview")
                .param("databaseId", "123").param("tableName", "test_table"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getTableStatisticTest() throws Exception {
        this.mockMvc.perform(get("/database-info/table-statistic")
                .param("databaseId", "123").param("tableName", "test_table"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
