package com.buralek.databasebrowser.api.databaseinfo.service;

import com.buralek.databasebrowser.api.databaseinfo.dto.ColumnDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.DataPreviewDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.SchemaDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableStatisticDto;
import com.buralek.databasebrowser.service.database.DatabaseUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class DatabaseInfoServiceTests {
    private final Integer DATABASE_ID = 1;
    private final String TABLE_NAME = "TEST_SCHEMA.ORDERS";

    @MockBean
    private DatabaseUtils databaseUtils;

    @Autowired
    private DatabaseInfoService databaseInfoService;

    @BeforeEach
    public void beforeAll() throws SQLException {
        when(databaseUtils.getConnection(DATABASE_ID)).thenReturn(getConnection());
    }

    @Test
    public void getSchemasTest() {
        final List<SchemaDto> schemaDtoList = databaseInfoService.getSchemas(DATABASE_ID);
        assertNotNull(schemaDtoList);
        assertEquals(schemaDtoList.size(), 3);
        assertEquals(schemaDtoList.get(2).getName(), "TEST_SCHEMA");
    }

    @Test
    public void getTablesTest() {
        final List<TableDto> tableDtoList = databaseInfoService.getTables(DATABASE_ID);

        assertNotNull(tableDtoList);
        assertEquals(tableDtoList.size(), 3);
        assertEquals(tableDtoList.get(1).getName(), "ORDERS");
        assertEquals(tableDtoList.get(2).getName(), "WAGES");
    }

    @Test
    public void getColumnsTest() {
        final List<ColumnDto> columnDtoList = databaseInfoService.getColumns(DATABASE_ID, TABLE_NAME);

        assertNotNull(columnDtoList);
        assertEquals(columnDtoList.size(), 3);
        assertEquals(columnDtoList.get(0).getColumnName(), "ID");
        assertEquals(columnDtoList.get(1).getColumnName(), "NAME");
        assertEquals(columnDtoList.get(2).getColumnName(), "PRICE");
    }

    @Test
    public void getDataPreviewTest() {
        final List<String> testData1 = new ArrayList<>();
        testData1.add("1");
        testData1.add("Iphone 9");
        testData1.add("100000");

        final List<String> testData2 = new ArrayList<>();
        testData2.add("2");
        testData2.add("Samsung S50");
        testData2.add("35000");

        final List<DataPreviewDto> dataPreviewDtoList = databaseInfoService.getDataPreview(DATABASE_ID, TABLE_NAME);

        assertNotNull(dataPreviewDtoList);
        assertEquals(dataPreviewDtoList.size(), 2);
        assertEquals(dataPreviewDtoList.get(0).getData(), testData1);
        assertEquals(dataPreviewDtoList.get(1).getData(), testData2);
    }

    @Test
    public void getDataStatisticTest() {
        final TableStatisticDto tableStatisticDto = databaseInfoService.getDataStatistic(DATABASE_ID, TABLE_NAME);

        assertEquals(tableStatisticDto.getNumberOfAttributes(), 3);
        assertEquals(tableStatisticDto.getNumberOfRecords(), 2);
    }

    private Connection getConnection() throws SQLException {
        final String url = "jdbc:h2:mem:sample_db";
        Properties properties = new Properties( );
        properties.put("user", "test_user");
        properties.put("password", "test_password");
        return  DriverManager.getConnection(url, properties);
    }

}
