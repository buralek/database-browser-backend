package com.buralek.databasebrowser.api.databaseinfo.dto;

import lombok.Data;

import java.util.List;

@Data
public class DataPreviewDto {
    private List<String> data;
}
