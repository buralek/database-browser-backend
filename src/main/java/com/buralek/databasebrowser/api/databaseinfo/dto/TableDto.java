package com.buralek.databasebrowser.api.databaseinfo.dto;

import lombok.Data;

@Data
public class TableDto {
    private String name;
    private String type;
    private String schema;
    private String catalog;
}
