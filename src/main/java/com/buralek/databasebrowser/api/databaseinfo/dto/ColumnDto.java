package com.buralek.databasebrowser.api.databaseinfo.dto;

import lombok.Data;

@Data
public class ColumnDto {
    private boolean isAutoIncrement;
    private boolean isCaseSensitive;
    private boolean isSearchable;
    private boolean isCurrency;
    private Integer isNullable;
    private boolean isSigned;
    private String columnLabel;
    private String columnName;
    private String schemaName;
    private Integer precision;
    private Integer scale;
    private String tableName;
    private String catalogName;
    private Integer columnType;
    private String columnTypeName;
    private boolean isReadonly;
    private boolean isWritable;
    private boolean isDefinitelyWritable;
    private String columnClassName;
}
