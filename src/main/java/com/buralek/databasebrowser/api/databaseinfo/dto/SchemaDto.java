package com.buralek.databasebrowser.api.databaseinfo.dto;

import lombok.Data;

@Data
public class SchemaDto {
    private String name;
    private String catalog;
}
