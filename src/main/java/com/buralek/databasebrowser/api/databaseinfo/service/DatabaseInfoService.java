package com.buralek.databasebrowser.api.databaseinfo.service;

import com.buralek.databasebrowser.api.databaseinfo.dto.ColumnDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.DataPreviewDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.SchemaDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableStatisticDto;

import java.util.List;

public interface DatabaseInfoService {
    List<SchemaDto> getSchemas(Integer databaseId);
    List<TableDto> getTables(Integer databaseId);
    List<ColumnDto> getColumns(Integer databaseId, String tableName);
    List<DataPreviewDto> getDataPreview(Integer databaseId, String tableName);
    TableStatisticDto getDataStatistic(Integer databaseId, String tableName);
}
