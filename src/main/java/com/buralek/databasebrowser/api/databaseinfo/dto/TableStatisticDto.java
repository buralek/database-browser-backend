package com.buralek.databasebrowser.api.databaseinfo.dto;

import lombok.Data;

@Data
public class TableStatisticDto {
    private Integer numberOfRecords;
    private Integer numberOfAttributes;
}
