package com.buralek.databasebrowser.api.databaseinfo.controller;

import com.buralek.databasebrowser.api.databaseinfo.dto.ColumnDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.DataPreviewDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.SchemaDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableStatisticDto;
import com.buralek.databasebrowser.api.databaseinfo.service.DatabaseInfoService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("database-info")
@RequiredArgsConstructor
public class DatabaseInfoController {
    private final DatabaseInfoService databaseInfoService;

    @GetMapping("schemas")
    @Operation(summary = "Get database schemas")
    public ResponseEntity<List<SchemaDto>> getSchemas(@RequestParam final Integer databaseId) {
        log.info("Start getting all database schemas by id '{}'", databaseId);
        final List<SchemaDto> schemaDtoList = databaseInfoService.getSchemas(databaseId);
        log.info("Database schemas are '{}'", schemaDtoList);
        return new ResponseEntity<>(schemaDtoList, HttpStatus.OK);
    }

    @GetMapping("tables")
    @Operation(summary = "Get database tables")
    public ResponseEntity<List<TableDto>> getTables(@RequestParam final Integer databaseId) {
        log.info("Start getting all tables by id '{}'", databaseId);
        final List<TableDto> tableDtoList = databaseInfoService.getTables(databaseId);
        log.info("Database tables are '{}'", tableDtoList);
        return new ResponseEntity<>(tableDtoList, HttpStatus.OK);
    }

    @GetMapping("columns")
    @Operation(summary = "Get columns from a table")
    public ResponseEntity<List<ColumnDto>> getColumnForTable(@RequestParam final Integer databaseId,
                                                            @RequestParam final String tableName) {
        log.info("Start getting all columns from table '{}' and database with id '{}'", tableName, databaseId);
        final List<ColumnDto> columnDtoList = databaseInfoService.getColumns(databaseId, tableName);
        log.info("Columns are'{}'", columnDtoList);
        return new ResponseEntity<>(columnDtoList, HttpStatus.OK);
    }

    @GetMapping("table-preview")
    @Operation(summary = "Get table data preview")
    public ResponseEntity<List<DataPreviewDto>> getTableDataPreview(@RequestParam final Integer databaseId,
                                                             @RequestParam final String tableName) {
        log.info("Start getting data preview from table '{}' and database with id '{}'", tableName, databaseId);
        final List<DataPreviewDto> dataPreview = databaseInfoService.getDataPreview(databaseId, tableName);
        log.info("Data preview is '{}'", dataPreview);
        return new ResponseEntity<>(dataPreview, HttpStatus.OK);
    }

    @GetMapping("table-statistic")
    @Operation(summary = "Get table statistic")
    public ResponseEntity<TableStatisticDto> getTableStatistic(@RequestParam final Integer databaseId,
                                                               @RequestParam final String tableName) {
        log.info("Start getting statistic of table '{}' and database with id '{}'", tableName, databaseId);
        final TableStatisticDto tableStatisticDto = databaseInfoService.getDataStatistic(databaseId, tableName);
        log.info("Table statistic is '{}'", tableStatisticDto);
        return new ResponseEntity<>(tableStatisticDto, HttpStatus.OK);
    }
}
