package com.buralek.databasebrowser.api.databaseinfo.service;

import com.buralek.databasebrowser.api.databaseinfo.dto.ColumnDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.DataPreviewDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.SchemaDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableDto;
import com.buralek.databasebrowser.api.databaseinfo.dto.TableStatisticDto;
import com.buralek.databasebrowser.service.database.DatabaseUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DatabaseInfoServiceImpl implements DatabaseInfoService {
    private static final String[] TABLE_TYPES = {"TABLE"};

    private final DatabaseUtils databaseUtils;

    @Override
    public List<SchemaDto> getSchemas(final Integer databaseId) {
        try(final Connection connection = databaseUtils.getConnection(databaseId)) {
            final ResultSet schemasResultSet = connection.getMetaData().getSchemas();
            final List<SchemaDto> schemaDtoList = new ArrayList<>();
            while(schemasResultSet.next()) {
                final SchemaDto schemaDto = new SchemaDto();
                schemaDto.setName(schemasResultSet.getString(1));
                schemaDto.setCatalog(schemasResultSet.getString(2));
                schemaDtoList.add(schemaDto);
            }
            return schemaDtoList;
        } catch (SQLException exception) {
            throw new RuntimeException("Can't connect to database " + exception);
        }
    }

    @Override
    public List<TableDto> getTables(final Integer databaseId) {
        try(final Connection connection = databaseUtils.getConnection(databaseId)) {
            final ResultSet schemasResultSet = connection.getMetaData().getTables(null, null, null, TABLE_TYPES);
            final List<TableDto> tableDtoList = new ArrayList<>();
            while(schemasResultSet.next()) {
                final TableDto tableDto = new TableDto();
                tableDto.setName(schemasResultSet.getString("TABLE_NAME"));
                tableDto.setType(schemasResultSet.getString("TABLE_TYPE"));
                tableDto.setSchema(schemasResultSet.getString("TABLE_SCHEM"));
                tableDto.setCatalog(schemasResultSet.getString("TABLE_CAT"));
                tableDtoList.add(tableDto);
            }
            return tableDtoList;
        } catch (SQLException exception) {
            throw new RuntimeException("Can't connect to database " + exception);
        }
    }

    @Override
    public List<ColumnDto> getColumns(final Integer databaseId, final String tableName) {
        try(final Connection connection = databaseUtils.getConnection(databaseId)) {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery("select * from " + tableName);
            final ResultSetMetaData metadata = resultSet.getMetaData();
            int columnCount = metadata.getColumnCount();

            final List<ColumnDto> columnDtoList = new ArrayList<>();
            for (int i = 1; i < columnCount + 1; i++) {
                final ColumnDto columnDto = new ColumnDto();
                columnDto.setColumnName(metadata.getColumnName(i));
                columnDto.setColumnType(metadata.getColumnType(i));
                columnDto.setAutoIncrement(metadata.isAutoIncrement(i));
                columnDto.setCaseSensitive(metadata.isCaseSensitive(i));
                columnDto.setSearchable(metadata.isSearchable(i));
                columnDto.setCurrency(metadata.isCurrency(i));
                columnDto.setIsNullable(metadata.isNullable(i));
                columnDto.setSigned(metadata.isSigned(i));
                columnDto.setColumnLabel(metadata.getColumnLabel(i));
                columnDto.setSchemaName(metadata.getSchemaName(i));
                columnDto.setPrecision(metadata.getPrecision(i));
                columnDto.setScale(metadata.getScale(i));
                columnDto.setTableName(metadata.getTableName(i));
                columnDto.setCatalogName(metadata.getCatalogName(i));
                columnDto.setColumnTypeName(metadata.getColumnTypeName(i));
                columnDto.setReadonly(metadata.isReadOnly(i));
                columnDto.setWritable(metadata.isWritable(i));
                columnDto.setDefinitelyWritable(metadata.isDefinitelyWritable(i));
                columnDto.setColumnClassName(metadata.getColumnClassName(i));
                columnDtoList.add(columnDto);
            }
            return columnDtoList;
        } catch (SQLException exception) {
            throw new RuntimeException("Can't connect to database " + exception);
        }
    }

    @Override
    public List<DataPreviewDto> getDataPreview(final Integer databaseId, final String tableName) {
        try(final Connection connection = databaseUtils.getConnection(databaseId)) {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery("select * from " + tableName);
            final ResultSetMetaData metadata = resultSet.getMetaData();
            int columnCount = metadata.getColumnCount();

            final List<DataPreviewDto> dataPreviewDtoList = new ArrayList<>();
            while (resultSet.next()) {
                final DataPreviewDto dataPreviewDto = new DataPreviewDto();
                final List<String> data = new ArrayList<>();
                for (int i = 1; i < columnCount + 1; i++) {
                    data.add(resultSet.getString(i));
                }
                dataPreviewDto.setData(data);
                dataPreviewDtoList.add(dataPreviewDto);
            }
            return dataPreviewDtoList;
        } catch (SQLException exception) {
            throw new RuntimeException("Can't connect to database " + exception);
        }
    }

    @Override
    public TableStatisticDto getDataStatistic(Integer databaseId, String tableName) {
        try(final Connection connection = databaseUtils.getConnection(databaseId)) {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery("select * from " + tableName);
            final ResultSetMetaData metadata = resultSet.getMetaData();
            int columnCount = metadata.getColumnCount();

            TableStatisticDto tableStatisticDto = new TableStatisticDto();
            tableStatisticDto.setNumberOfAttributes(columnCount);

            int rowCounter = 0;
            while (resultSet.next()) {
                rowCounter++;
            }
            tableStatisticDto.setNumberOfRecords(rowCounter);
            return tableStatisticDto;
        } catch (SQLException exception) {
            throw new RuntimeException("Can't connect to database " + exception);
        }
    }
}
