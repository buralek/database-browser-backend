package com.buralek.databasebrowser.api.connectiondetails.dto;

import lombok.Data;

@Data
public class UpdateConnectionDetailRequest {
    private String name;

    private String hostname;

    private String port;

    private String databaseName;

    private String username;

    private String password;
}
