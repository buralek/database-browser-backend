package com.buralek.databasebrowser.api.connectiondetails.service;

import com.buralek.databasebrowser.api.connectiondetails.dto.ConnectionDetailDto;
import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;

public interface ConnectionDetailDtoMapper {
    ConnectionDetailDto map(ConnectionDetailEntity entity);
}
