package com.buralek.databasebrowser.api.connectiondetails.service.impl;

import com.buralek.databasebrowser.api.connectiondetails.dto.CreateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.service.CreateConnectionDetailDtoMapper;
import com.buralek.databasebrowser.service.crypt.ObjectCrypter;
import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static ch.qos.logback.core.encoder.ByteArrayUtil.toHexString;

@Service
@RequiredArgsConstructor
public class CreateConnectionDetailDtoMapperImpl implements CreateConnectionDetailDtoMapper {
    private final ObjectCrypter objectCrypter;

    @Override
    public ConnectionDetailEntity map(CreateConnectionDetailRequest dto) {
        ConnectionDetailEntity entity = new ConnectionDetailEntity();
        entity.setDatabaseName(dto.getDatabaseName());
        entity.setName(dto.getName());
        entity.setHostname(dto.getHostname());
        entity.setPort(dto.getPort());
        entity.setUsername(toHexString(objectCrypter.encrypt(dto.getUsername())));
        entity.setPassword(toHexString(objectCrypter.encrypt(dto.getPassword())));
        return entity;
    }
}
