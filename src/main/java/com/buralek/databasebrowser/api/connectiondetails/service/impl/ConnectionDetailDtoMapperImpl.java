package com.buralek.databasebrowser.api.connectiondetails.service.impl;

import com.buralek.databasebrowser.api.connectiondetails.dto.ConnectionDetailDto;
import com.buralek.databasebrowser.api.connectiondetails.service.ConnectionDetailDtoMapper;
import com.buralek.databasebrowser.service.crypt.ObjectCrypter;
import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static ch.qos.logback.core.encoder.ByteArrayUtil.hexStringToByteArray;

@Service
@RequiredArgsConstructor
public class ConnectionDetailDtoMapperImpl implements ConnectionDetailDtoMapper {
    private final ObjectCrypter objectCrypter;

    @Override
    public ConnectionDetailDto map(final ConnectionDetailEntity entity) {
        final ConnectionDetailDto dto = new ConnectionDetailDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setHostname(entity.getHostname());
        dto.setPort(entity.getPort());
        dto.setDatabaseName(entity.getDatabaseName());
        dto.setUsername((String)objectCrypter.decrypt(hexStringToByteArray(entity.getUsername())));
        dto.setPassword((String)objectCrypter.decrypt(hexStringToByteArray(entity.getPassword())));
        return dto;
    }
}
