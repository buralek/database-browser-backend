package com.buralek.databasebrowser.api.connectiondetails.controller;

import com.buralek.databasebrowser.api.connectiondetails.dto.ConnectionDetailDto;
import com.buralek.databasebrowser.api.connectiondetails.dto.CreateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.dto.UpdateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.service.ConnectionDetailsService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("connection-detail")
public class ConnectionDetailsController {

    private final ConnectionDetailsService connectionDetailsService;

    @GetMapping("all")
    @Operation(summary = "Get connection details about all storable databases")
    public ResponseEntity<List<ConnectionDetailDto>> getAllConnectionDetails() {
        log.info("Start getting all connection details");
        final List<ConnectionDetailDto> connectionDetailDtoList = connectionDetailsService.getAllConnectionDetails();
        log.info("All connection details are '{}'", connectionDetailDtoList);
        return new ResponseEntity<>(connectionDetailDtoList, HttpStatus.OK);
    }

    @GetMapping
    @Operation(summary = "Get connection detail by id")
    public ResponseEntity<ConnectionDetailDto> getConnectionDetailById(
            @RequestParam final Integer id) {
        log.info("Start getting connection details by id '{}'", id);
        final ConnectionDetailDto connectionDetailDto = connectionDetailsService.getConnectionDetailById(id);
        log.info("Found connection detail is '{}'", connectionDetailDto);
        return new ResponseEntity<>(connectionDetailDto, HttpStatus.OK);
    }

    @PostMapping
    @Operation(summary = "Create new connection detail")
    public ResponseEntity<ConnectionDetailDto> createConnectionDetail(@RequestBody final CreateConnectionDetailRequest createConnectionDetailRequest) {
        log.info("Start creating new connection details from request{}'", createConnectionDetailRequest);
        final ConnectionDetailDto connectionDetailDto = connectionDetailsService.createConnectionDetail(createConnectionDetailRequest);
        log.info("Created connection detail is '{}'", connectionDetailDto);
        return new ResponseEntity<>(connectionDetailDto, HttpStatus.CREATED);
    }

    @PutMapping
    @Operation(summary = "Update connection detail")
    public ResponseEntity<ConnectionDetailDto> updateConnectionDetailById(
            @RequestParam final  Integer id,
            @RequestBody final UpdateConnectionDetailRequest updateRequest) {
        log.info("Start updating connection details with id '{}' from request '{}'", id, updateRequest);
        final ConnectionDetailDto updatedConnectionDetailDto = connectionDetailsService.updateConnectionDetailById(id, updateRequest);
        log.info("Updated connection detail is '{}'", updatedConnectionDetailDto);
        return new ResponseEntity<>(updatedConnectionDetailDto, HttpStatus.OK);
    }

    @DeleteMapping
    @Operation(summary = "Delete connection detail by id")
    public ResponseEntity deleteConnectionDetailById(
            @RequestParam Integer id) {
        log.info("Start deleting connection details by id '{}'", id);
        connectionDetailsService.deleteConnectionDetailById(id);
        log.info("Connection detail with id '{}' is deleted", id);
        return ResponseEntity.ok().build();
    }
}
