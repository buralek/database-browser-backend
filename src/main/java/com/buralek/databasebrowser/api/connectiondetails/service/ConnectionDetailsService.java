package com.buralek.databasebrowser.api.connectiondetails.service;

import com.buralek.databasebrowser.api.connectiondetails.dto.ConnectionDetailDto;
import com.buralek.databasebrowser.api.connectiondetails.dto.CreateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.dto.UpdateConnectionDetailRequest;

import java.util.List;

public interface ConnectionDetailsService {
    List<ConnectionDetailDto> getAllConnectionDetails();
    ConnectionDetailDto getConnectionDetailById(Integer id);
    ConnectionDetailDto createConnectionDetail(CreateConnectionDetailRequest connectionDetailDto);
    void deleteConnectionDetailById(Integer id);
    ConnectionDetailDto updateConnectionDetailById(Integer id, UpdateConnectionDetailRequest updateConnectionDetailRequest);
}
