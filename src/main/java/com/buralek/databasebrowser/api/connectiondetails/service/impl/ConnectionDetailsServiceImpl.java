package com.buralek.databasebrowser.api.connectiondetails.service.impl;

import com.buralek.databasebrowser.api.connectiondetails.dto.ConnectionDetailDto;
import com.buralek.databasebrowser.api.connectiondetails.dto.CreateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.dto.UpdateConnectionDetailRequest;
import com.buralek.databasebrowser.api.connectiondetails.service.ConnectionDetailDtoMapper;
import com.buralek.databasebrowser.api.connectiondetails.service.ConnectionDetailsService;
import com.buralek.databasebrowser.api.connectiondetails.service.CreateConnectionDetailDtoMapper;
import com.buralek.databasebrowser.service.crypt.ObjectCrypter;
import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;
import com.buralek.databasebrowser.service.database.repository.ConnectionDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ConnectionDetailsServiceImpl implements ConnectionDetailsService {
    private final ConnectionDetailDtoMapper connectionDetailDtoMapper;
    private final ConnectionDetailsRepository connectionDetailsRepository;
    private final CreateConnectionDetailDtoMapper createConnectionDetailDtoMapper;

    @Override
    public List<ConnectionDetailDto> getAllConnectionDetails() {
        return connectionDetailsRepository.findAll().stream()
                .map(connectionDetailDtoMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public ConnectionDetailDto getConnectionDetailById(final Integer id) {
        final ConnectionDetailEntity connectionDetailEntity = connectionDetailsRepository.findById(id).orElse(new ConnectionDetailEntity());
        return connectionDetailDtoMapper.map(connectionDetailEntity);
    }

    @Override
    public ConnectionDetailDto createConnectionDetail(final CreateConnectionDetailRequest connectionDetailDto) {
        final ConnectionDetailEntity connectionDetailEntity = connectionDetailsRepository.save(createConnectionDetailDtoMapper.map(connectionDetailDto));
        return connectionDetailDtoMapper.map(connectionDetailEntity);
    }

    @Override
    public void deleteConnectionDetailById(final Integer id) {
        connectionDetailsRepository.deleteById(id);
    }

    @Override
    public ConnectionDetailDto updateConnectionDetailById(final Integer id, final UpdateConnectionDetailRequest updateConnectionDetailRequest) {
        final ConnectionDetailEntity connectionDetailEntity = connectionDetailsRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Can't find connection details with id '" + id + "'"));
        if (updateConnectionDetailRequest.getName() != null) {
            connectionDetailEntity.setName(updateConnectionDetailRequest.getName());
        }
        if (updateConnectionDetailRequest.getHostname() != null) {
            connectionDetailEntity.setHostname(updateConnectionDetailRequest.getHostname());
        }
        if (updateConnectionDetailRequest.getPort() != null) {
            connectionDetailEntity.setPort(updateConnectionDetailRequest.getPort());
        }
        if (updateConnectionDetailRequest.getDatabaseName() != null) {
            connectionDetailEntity.setDatabaseName(updateConnectionDetailRequest.getDatabaseName());
        }
        if (updateConnectionDetailRequest.getUsername() != null) {
            connectionDetailEntity.setUsername(updateConnectionDetailRequest.getUsername());
        }
        if (updateConnectionDetailRequest.getPassword() != null) {
            connectionDetailEntity.setPassword(updateConnectionDetailRequest.getPassword());
        }
        return connectionDetailDtoMapper.map(connectionDetailsRepository.save(connectionDetailEntity));
    }

}
