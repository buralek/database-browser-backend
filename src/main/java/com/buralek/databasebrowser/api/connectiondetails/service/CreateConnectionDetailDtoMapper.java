package com.buralek.databasebrowser.api.connectiondetails.service;

import com.buralek.databasebrowser.api.connectiondetails.dto.CreateConnectionDetailRequest;
import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;

public interface CreateConnectionDetailDtoMapper {
    ConnectionDetailEntity map(CreateConnectionDetailRequest dto);
}
