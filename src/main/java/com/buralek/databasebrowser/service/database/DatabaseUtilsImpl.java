package com.buralek.databasebrowser.service.database;

import com.buralek.databasebrowser.service.crypt.ObjectCrypter;
import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;
import com.buralek.databasebrowser.service.database.repository.ConnectionDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static ch.qos.logback.core.encoder.ByteArrayUtil.hexStringToByteArray;
import static ch.qos.logback.core.encoder.ByteArrayUtil.toHexString;

@Service
@RequiredArgsConstructor
@Slf4j
public class DatabaseUtilsImpl implements DatabaseUtils {
    private final ObjectCrypter objectCrypter;
    private final String POSTGRESQL = "jdbc:postgresql://";
    private final String USER = "user";
    private final String PASSWORD = "password";

    private final ConnectionDetailsRepository connectionDetailsRepository;

    @Override
    public Connection getConnection(final Integer databaseId) throws SQLException {
        ConnectionDetailEntity connectionDetailEntity = connectionDetailsRepository.findById(databaseId)
                .orElseThrow(() -> new RuntimeException("Can't find database connection details by id '" + databaseId + "'"));

        String url = POSTGRESQL
                + connectionDetailEntity.getHostname()
                + ":" + connectionDetailEntity.getPort()
                + "/" + connectionDetailEntity.getDatabaseName();

        Properties properties = new Properties( );
        properties.put(USER, objectCrypter.decrypt(hexStringToByteArray(connectionDetailEntity.getUsername())));
        properties.put(PASSWORD, objectCrypter.decrypt(hexStringToByteArray(connectionDetailEntity.getPassword())));
        return  DriverManager.getConnection(url, properties);
    }
}
