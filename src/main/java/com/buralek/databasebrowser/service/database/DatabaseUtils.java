package com.buralek.databasebrowser.service.database;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseUtils {
    Connection getConnection(Integer databaseId)  throws SQLException;
}
