package com.buralek.databasebrowser.service.database.repository;

import com.buralek.databasebrowser.service.database.model.ConnectionDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionDetailsRepository extends JpaRepository<ConnectionDetailEntity, Integer> {
}
