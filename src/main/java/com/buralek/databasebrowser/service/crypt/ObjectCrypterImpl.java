package com.buralek.databasebrowser.service.crypt;

import com.buralek.databasebrowser.config.ServiceSettingsConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static ch.qos.logback.core.encoder.ByteArrayUtil.hexStringToByteArray;

@Service
@Slf4j
public class ObjectCrypterImpl implements ObjectCrypter {
    private final Cipher deCipher;
    private final Cipher enCipher;
    private final SecretKeySpec key;
    private final IvParameterSpec ivSpec;

    private final ServiceSettingsConfig serviceSettingsConfig;

    public ObjectCrypterImpl(final ServiceSettingsConfig serviceSettingsConfig) {
        this.serviceSettingsConfig = serviceSettingsConfig;

        byte[] ivBytes = hexStringToByteArray(serviceSettingsConfig.getIvBytesHex());
        byte[] keyBytes = hexStringToByteArray(serviceSettingsConfig.getKeyBytesHex());

        ivSpec = new IvParameterSpec(ivBytes);
        try {
            final DESKeySpec dkey = new  DESKeySpec(keyBytes);
            key = new SecretKeySpec(dkey.getKey(), "DES");
            deCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            enCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException exception) {
            throw new RuntimeException("Crypt exception " + exception);
        }
    }

    public byte[] encrypt(Object obj) {
        try {
            byte[] input = convertToByteArray(obj);
            enCipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
            return enCipher.doFinal(input);
        } catch(InvalidKeyException | InvalidAlgorithmParameterException | IOException
                | IllegalBlockSizeException  | BadPaddingException exception) {
            throw new RuntimeException("Crypt exception " + exception);
        }
    }

    public Object decrypt( byte[]  encrypted) {
        try {
            deCipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
            return convertFromByteArray(deCipher.doFinal(encrypted));
        } catch(InvalidKeyException | InvalidAlgorithmParameterException | IOException
                | IllegalBlockSizeException  | BadPaddingException | ClassNotFoundException exception) {
            throw new RuntimeException("Crypt exception " + exception);
        }
    }

    private Object convertFromByteArray(byte[] byteObject) throws IOException,
            ClassNotFoundException {
        final ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(byteObject));
        final Object object = in.readObject();
        in.close();
        return object;
    }

    private byte[] convertToByteArray(Object complexObject) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final ObjectOutputStream out = new ObjectOutputStream(baos);
        out.writeObject(complexObject);
        out.close();
        return baos.toByteArray();
    }
}
