package com.buralek.databasebrowser.service.crypt;

public interface ObjectCrypter {
    Object decrypt( byte[]  encrypted);
    byte[] encrypt(Object obj);
}
