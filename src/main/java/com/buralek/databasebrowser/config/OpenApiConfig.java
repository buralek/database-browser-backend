package com.buralek.databasebrowser.config;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Database browser project", version = "${service-settings.version}"))
public class OpenApiConfig {
}
