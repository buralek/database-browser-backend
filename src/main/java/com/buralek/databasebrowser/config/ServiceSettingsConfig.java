package com.buralek.databasebrowser.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ServiceSettingsConfig {
    @Value("${service-settings.ivBytes}")
    private String ivBytesHex;

    @Value("${service-settings.keyBytes}")
    private String keyBytesHex;
}
